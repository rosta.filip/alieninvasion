class Settings:
    """ Class to store all settings for alien invasion game. """

    def __init__(self):
        """Initialize the game's static settings. """
        # Screen settings
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (230, 230, 230)

        # Ship settings
        self.ship_limit = 3

        # Bullet settings
        self.bullet_width = 3  # for testing 1900 (other 3)
        self.bullet_height = 15
        self.bullet_color = (60, 60, 60)
        self.bullet_allowed = 4

        # Alien settings.
        self.fleet_drop_speed = 5  # for testing 100 (other 5 or 10)

        # How quickly the game speeds up.
        self.speed_scale = 1.1

        # How quickly the alien point values increase.
        self.score_scale = 1.5

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """Initialize the settings that change through the game. """
        self.ship_speed = 1.5
        self.bullet_speed = 1
        self.alien_speed = 1
        self.fleet_direction = 1  # 1 represent right, -1 represent left

        # Scoring
        self.alien_points = 20

    def increase_speed(self):
        """Increase speed settings and alien point value. """
        self.ship_speed *= self.speed_scale
        self.bullet_speed *= self.speed_scale
        self.alien_speed *= self.speed_scale

        self.alien_points = int(self.alien_points * self.score_scale)
